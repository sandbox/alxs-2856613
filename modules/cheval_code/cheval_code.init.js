/**
 * @file
 * Provides Cheval code block attachment.
 */

(function ($, Drupal) {

  "use strict";

  Drupal.behaviors.cheval_code = {
    attach: function (context, settings) {
      var _ = this;
      var $code = 0;
      $("code", context).each(function() {
        $code++;
        var $this = this;
        $(this).wrap( "<span class='cheval-code-wrapper'></span>" );
        $(this).addClass('text-to-copy-' + $code);
        $('<span class="cheval-copy js-copy-btn-' + $code + '">Copy Code</span>').insertAfter($this);
      });
    },

  };

})(jQuery, Drupal);
